


// CUSTOMER CLASS
class Customer {
    constructor(email){
        this.email = email;
        this.cart = new Cart();
        this.orders = [];
    }

    checkOut(){
        if (this.cart !== undefined || this.cart !== ''){
            this.orders.push(this.cart);
        }
        return this;
    }
}

class Product {
    constructor(name,price){
        this.name = name;
        this.price = price;
        this.isActive = true;
    }

    archive(){
        if (this.isActive){
            this.isActive = false;
        }
        return this;
    }

    updatePrice(price){
        this.price = price;
        return this;
    }
    
}

class Cart {
    constructor() {
        this.contents = [];
        this.totalAmount = 0;

        this.quantity = undefined;
    }

    addtoCart(product, quantity){
       this.contents.push({
                product: product,
                quantity: quantity
        })
        return this;
    }

    showCartContents(){
        // console.log(this.contents);
        return this.contents;
    }

    updateProductQuantity(productName, updatedQuantity){
        for (let i = 0; i < this.contents.length; i++) {
            if(this.contents[i].product.name === productName) {
                this.contents[i].quantity = updatedQuantity;
                // console.log(this.contents);
            }
        } 
        this.quantity = updatedQuantity;
        return this;
    }

    clearCartContents(){
        this.contents = [];
        this.totalAmount = 0;
        this.quantity = undefined;
        return this;
    }

    computeTotal(){
        let total = 0;
        for (let i = 0; i < this.contents.length; i++) {
            total = this.contents[i].product.price * this.contents[i].quantity;
        }

        this.totalAmount = total;

        return this;
    }
}
let john = new Customer('john@mail.com');


let prodA = new Product('soap', 9.99);