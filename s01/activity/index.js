let students = ["John", "Joe", "Jane", "Jessie"];

// 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
let addToEnd = (str, arr) => {
    if (typeof str === 'string') {
       arr.push(str);
       return students;
    }
    else {
        return "error - can only add strings to an array"
    }
};

// 2. Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.

let addToStart = (str, arr) => {
    if (typeof str === 'string') {
       arr.unshift(str);
       return students;
    }
    else {
        return "error - can only add strings to an array"
    }
};

// 3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

let elementChecker = (name, arr) => {
    if (arr.includes(name)) {
       return true;
    }
    else if (arr.length === 0){
        return "error - passed in array is empty"
    }
    else {
        return false;
    }
};

// 4. Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:if array is empty, return "error - array must NOT be empty"if at least one array element is NOT a string, return "error - all array elements must be strings"if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"if every element in the array ends in the passed in character, return true. Otherwise return false. Use the students array and the character "e" as arguments when testing.

// let checkAllStringsEnding = (character, arr) => {
//     if (arr.length === 0) {
//         return "error - passed in array must NOT be empty";
//     }
//     else if (arr.forEach(element => {
        
//     })){
//         return "error - all array elements must be strings";
//     }
//     else if (){
//         return "error - 2nd argument must be of data type string";
//     }
//     else if () {
//         return "error - 2nd argument must be a single character";
//     } else if () {
//         return true;
//     } else {
//         return false;
//     }
// };

// 5. Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.

let stringLengthSorter = (arr) => {

    if(arr.every(i => typeof i === "string")){
        let sortedArr = arr.sort((a, b) => a.length - b.length);
        return sortedArr;
    }else {
    
        return "error - all array elements must be strings";
    }

    

};

// 6. Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:if array is empty, return "error - array must NOT be empty"if at least one array element is NOT a string, return "error - all array elements must be strings"if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"return the number of elements in the array that start with the character argument, must be case-insensitiveUse the students array and the character "J" as arguments when testing.

let startsWithCounter = (arr, char) => {
    let count;
    for (let i = 0; i < arr.length; i++) {
       for (let j = 0; j < arr.length[i]; j++) {
           if (arr.length[i][j] === char) {
               count = arr.length;
           }
       }
    }

    if (arr.length === 0) {
        return "error - array must NOT be empty";
    } else if(arr.every(i => typeof i !== "string")){
        return "all array elements must be strings";
    } else if (typeof char !== 'string') {
        return "error- 2nd argument must be of data type string"
    } else if (char.length > 1) {
        return "error - 2nd argument must be a single character"
    } else {
        return count;
    }
};

// 8. Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.

let randomPicker = (arr) => {
    return arr[Math.floor(arr.length * Math.random())];
}