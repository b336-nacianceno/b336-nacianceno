// Array is a predefined object in JS. As such, it is a collection of key and value pairs
// 0 : John, 1: Joe
let students = ["John", "Joe", "Jane", "Jessie"];


// Math has 8 pre-defined properties which can all be called via the syntax "Math.PROPERTY" (case-sensitive):

Math.E        // returns Euler's number
Math.PI       // returns PI
Math.SQRT2    // returns the square root of 2
Math.SQRT1_2  // returns the square root of 1/2
Math.LN2      // returns the natural logarithm of 2
Math.LN10     // returns the natural logarithm of 10
Math.LOG2E    // returns base 2 logarithm of E
Math.LOG10E   // returns base 10 logarithm of E

// Math methods are invoked with the syntax "Math.method(number)"
// Most commonly used Math methods are:

// Most commonly used:

Math.round(3.14) //3 - rounds to nearest integer
Math.ceil(3.14) //4 - rounds UP to nearest integer
Math.floor(3.14) //3 - rounds DOWN to nearest integer
Math.trunc(3.14) //3 - returns only the integer part (NEW in ES6)

// method for returning the square root of a number
Math.sqrt(3.14) //1.77

// method for finding the lowest value in a list of arguments
Math.min(-4, -3, -2, -1, 0, 1, 2, 3, 4) //-4

// method for finding the highest value in a list of arguments
Math.max(-4, -3, -2, -1, 0, 1, 2, 3, 4) //4

// method for returning a random number between 0 (inclusive) and 1 (exclusive)
Math.random()

// Objects vs Primitive
// Not everything in JS is an object. Primitive data types are NOT objects - no properties or methods

// Commonly used JS primitives: string, number, boolean, undefined, null

let name = "Mario";

// JS automatically wraps primitive data in an object to use common properties perform functions like .lengh and toUpperCase
// PD can still act like objects by creating a CUSTOM OBJECT