//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
function login(email){
    console.log(`${email} has logged in`);
}

function logout(email){
    console.log(`${email} has logged out`);
}

function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}

//This way of organizing students is not well organized at all.
//This will become unmanageable when we add more students or functions
//To remedy this, we will create objects

// ACTIVITY - Function Coding

let studentOne = {
    name: "John",
    email: "john@mail.com",
    grades: [89, 84, 78, 88],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass() {
        return this.computeAve() >= 85 ? true : false;
    },
    willPassWithHonors() {
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }
    

}


let studentTwo = {
    name: "Joe",
    email: "joe@mail.com",
    grades: [78, 82, 79, 85],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass() {
        return this.computeAve() >= 85 ? true : false;
    },
    willPassWithHonors() {
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }
    
    
}

let studentThree = {
    name: "Jane",
    email: "jane@mail.com",
    grades: [87, 89, 91, 93],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass() {
        return this.computeAve() >= 85 ? true : false;
    },
    willPassWithHonors() {
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }

}

let studentFour = {
    name: "Jessie",
    email: "jessie@mail.com",
    grades: [91, 89, 92, 93],
    login(){
        console.log(`${this.email} has logged in`);
    },
    logout(){
        console.log(`${this.email} has logged out`);
    },
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    },
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum/4;
    },
    willPass() {
        return this.computeAve() >= 85 ? true : false;
    },
    willPassWithHonors() {
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }

}


let classOf1A = {
    students: [
        studentOne, studentTwo, studentThree, studentFour
    ],
    countHonorStudents() {
        let honorStudents= 0;
        for (let i = 0; i < this.students.length; i++) {
            if (this.students[i].willPassWithHonors() === true) {
                honorStudents += 1;
            }
        }
        return honorStudents;
    },
    honorsPercentage(){
        let numberofStudents = 4;
        let honorStudents= 0;
        for (let i = 0; i < this.students.length; i++) {
            if (this.students[i].willPassWithHonors() === true) {
                honorStudents += 1;
            }
        }
        return ((honorStudents/numberofStudents)*100)+ "%";
    },
    retrieveHonorStudentInfo() {

        let honorStudentInfo = []
        for (let i = 0; i < this.students.length; i++) {
            if (this.students[i].willPassWithHonors() === true) {
                honorStudentInfo.push(
                {
                    email: this.students[i].email,
                    averageGrade: this.students[i].computeAve()
                });

            }
        }
        return honorStudentInfo;
    },
    sortHonorStudentsByGradeDesc() {
        let honorStudentInfo = [];
        let honorStudentGrades = [];
        for (let i = 0; i < this.students.length; i++) {
            if (this.students[i].willPassWithHonors() === true) {
                honorStudentInfo.push(
                {
                    email: this.students[i].email,
                    averageGrade: this.students[i].computeAve()
                });
            }
        }

        for (let i = 0; i < this.students.length; i++) {
            if (this.students[i].willPassWithHonors() === true) {
                honorStudentGrades.push(this.students[i].computeAve());
            }
        }
        // if (honorStudentInfo[0].averageGrade < );
        // sortedHonorStudents[0].averageGrade

        honorStudentGrades.sort((a,b) => a-b);
        
    }
}


