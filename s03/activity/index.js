// in JS, we define a class by using keyword "class" followed by "class name", and a set of {}

// Naming convention dictates that class names should begin with an uppercase letter
class Student {
    // "constructor" method defined HOW objects will be instantiated from a class
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;

        // Activity 1 START
        // Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.
        if(grades.length === 4) {
            if(grades.every(grade => grade >= 0 && grade <= 100)) {
                this.grades = grades
            }
        } else {
            this.grades = undefined;
        }

        // Activity 1 END
        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;

    }

    // Methods are defined OUTSIDE the constructor method and they are NOT separated by commas as with objects
    login() {
        console.log(`${this.email} has logged in`);
        return this;
    }

     logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }

     listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        // return sum/4;
        this.gradeAve = sum/4;
        return this;
    }

    willPass() {
        this.passed = this.computeAve() >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {
        this.passedWithHonors = (this.willPass() && this.computeAve() >= 90) ? true : false;
        return this;
    }

}

// To create objects from a class, we use the keyword "new"
// Object instantiation - technical term for this process
// let studentOne = new Student("John", "john@mail.com");
// let studentTwo = new Student();

// Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.
let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);

// studentOne.login().computeAve().willPass().willPassWitHonors().logout()

// studentOne.login().computeAve().logout()
/*
Quiz 1:
1. What is the blueprint where objects are created from? CLASS
2. What is the naming convention applied to classes? UPPER-CASE FIRST LETTER
3. What keyword do we use to create objects from a class? NEW
4. What is the technical term for creating an object from a class? OBJECT INSTANTIATION
5. What class method dictates HOW objects will be created from that class? CONSTRUCTOR
*/

// Updating a property's value via reassignment is allowed
// Best practice dictates that we regulate access to such properties. We do so via the use getters and setters








