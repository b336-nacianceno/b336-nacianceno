//  in JS, we define a class by using the keyword "class" followed by "className" and {}
// class name should always start with an upper case letter
class Student {
    // "constructor" method defines how objects will be instantiated from a class
    constructor(name, email, grades) {
        this.name = name;
        this.email = email;
        // 1. Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.
        if(grades.length === 4) {
            if(grades.every(grade => grade >=0 && grade <= 100)) {
                this.grades == grades
            }
        } else {
            this.grades = undefined;
        }
        // 1. end
        this.gradeAve = undefined;
    }

    // Methods are defined OUTSIDE the constructor method and they are NOT separated by commas as with objects
    login() {
        console.log(`${this.email} has logged in`);
        return this;
    }
    
    logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }
    
    listGrades() {
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    
    }
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        // return sum/4;
        this.gradeAve = sum/4;
        return this;
    }
    
    willPass() {
        return this.computeAve() >= 85 ? true : false;
    }
    
    willPassWithHonors() {
        return (this.willPass() && this.computeAve() >= 90) ? true : false;
    }
        
}

// To create objects from a class, new use the keyword "new"
// object instantiation
// let studentOne = new Student("John","john@mail.com");
// let studentTwo = new Student();


// Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.
let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);

// studentOne.login().listGrades().logout();
// sequence of methods - successful only if methods return objects