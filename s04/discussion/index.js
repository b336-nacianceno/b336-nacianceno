// Nested clases
// what if we want to make an object representing a group of students

// nesting within a Student class

class Student {
    constructor(name, email, grades){
        this.name = name;
        this.email = email;
        if(grades.length === 4){
            if(grades.every(grade => grade >= 0 && grade <= 100)){
                this.grades = grades;
            } else {
                this.grades = undefined;
            }
        } else {
            this.grades = undefined;
        }

        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;
        this.honorsPercentage = undefined;
    }

    login(){
        console.log(`${this.email} has logged in`);
        return this;
    }
    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }
    listGrades(){
        console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
        return this;
    }
    computeAve(){
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        this.gradeAve = sum/4;
        return this;
    }
    willPass() {
        this.passed = this.computeAve().gradeAve >= 85 ? true : false;
        return this;
    }
    willPassWithHonors() {
        if (this.passed) {
            if (this.gradeAve >= 90) {
                this.passedWithHonors = true;
            } else {
                this.passedWithHonors = false;
            }
        } else {
            this.passedWithHonors = false;
        }
        return this;
    }
}

class Section {
    constructor (name) {
        this.name = name;
        this.students = [];
        this.honorStudents = undefined;
    }

    addStudent(name, email, grades){
        this.students.push(new Student(name, email, grades));
        return this;
    }

    // method for computing how many students in the section are honor students

    countHonorStudents(){
        let count = 0;
        this.students.forEach(student => {
            // will increment count if 3 methods returned true
            // .passedWithHonors (property) itself, if true
            // .computeAve().willPass() are not required but to make it more readable - these methods are already inside .willPassWithHonors() 
            // alternative: if(student.willPassWithHonors().passWithHonors)
            if(student.computeAve().willPass().willPassWithHonors().passedWithHonors) {
                count++;
            }
        })
        this.honorStudents = count;
        return this;
    }

    // Method for computing how many students in the section are honor students
    // section1A.countHonorStudents().computeHonorsPercentage()
    computeHonorsPercentage(){
        // (number of honorStudents divided by no. of students) * 100
        this.honorsPercentage = (this.honorStudents / this.students.length) * 100;
        return this;
    }
}


// create new Section classes
const section1A = new Section('section1A');
const section1B = new Section('section1B');
const section1C = new Section('section1C');
const section1D = new Section('section1D');

// 'John', 'john@mail.com', [89, 84, 78, 88]
// nested class

// section1A
section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

// section1B
section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

// section1C
section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

// section1D
section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);

// a student object in the students array property accessed via its index
section1A.students[0];

// access method in the nested object
section1A.students[0].computeAve();
